from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

# Init app
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
# Database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'db.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Init db
db = SQLAlchemy(app)

# Init ma
ma = Marshmallow(app)

# Student Class/Model
class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    rollno = db.Column(db.String(20))
    degree = db.Column(db.String(20))
    course = db.Column(db.String(30))
    cgpa = db.Column(db.Float)

    def __init__(self, name, rollno, degree, course, cgpa):
        self.name = name
        self.rollno = rollno
        self.degree = degree
        self.course = course
        self.cgpa = cgpa

# Student Schema
class StudentSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'rollno', 'degree', 'course', 'cgpa')

# Init Schema
student_schema = StudentSchema()
students_schema = StudentSchema(many=True)

# Create a student
@app.route('/student', methods=['POST'])
def add_student():
    name = request.json['name']
    rollno = request.json['rollno']
    degree = request.json['degree']
    course = request.json['course']
    cgpa = request.json['cgpa']

    new_student = Student(name, rollno, degree, course, cgpa)

    db.session.add(new_student)
    db.session.commit()

    return student_schema.jsonify(new_student)

# Get all students
@app.route('/students', methods=['GET'])
def get_students():
    all_students = Student.query.all()
    result = students_schema.dump(all_students)
    return jsonify(result)


# Get single student
@app.route('/student/<id>', methods=['GET'])
def get_student(id):
    student = Student.query.get(id)
    return student_schema.jsonify(student)


# Update student
@app.route('/student/<id>', methods=['PUT'])
def update_student(id):
    student = Student.query.get(id)

    name = request.json['name']
    rollno = request.json['rollno']
    degree = request.json['degree']
    course = request.json['course']
    cgpa = request.json['cgpa']

    student.name = name
    student.rollno = rollno
    student.degree = degree
    student.course = course
    student.cgpa = cgpa

    db.session.commit()

    return student_schema.jsonify(student)


# Delete student
@app.route('/student/<id>', methods=['DELETE'])
def delete_student(id):
    student = Student.query.get(id)
    db.session.delete(student)
    db.session.commit()
    return student_schema.jsonify(student)



# Run server
if __name__ == '__main__':
    app.run(debug=True)

