## Build a docker image
`docker build -t <imageName:version> .`

## Run a docker container in daemon mode with ports exposed
`docker run -it -d -p <outsidePort>:5000 <imageName:version>`

API Endpoints
- GET /students 
- GET /student/<integer_id>
- POST /student
- PUT /student/<integer_id>
- DELETE /student/<integer_id>




        

        

        
        